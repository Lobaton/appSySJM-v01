package sys_jm.app.tiendaonline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Sugerencia extends AppCompatActivity {
    protected String CorreoSYSJM = "ventas@seguridadsistema.com.ve";
    protected String Asunto = "Sugerencia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sugerencia);

        Herramienta H = new Herramienta();

        Button boton = (Button) findViewById(R.id.button);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TextView Correo = (TextView) findViewById(R.id.text_correo);
                TextView Nombre = (TextView) findViewById(R.id.text_nombre);
                TextView Obser = (TextView) findViewById(R.id.text_obser);

                Intent itSend = new Intent(Intent.ACTION_SEND);
                itSend.setType("plain/text");

                //colocamos los datos para el envío
                itSend.putExtra(Intent.EXTRA_EMAIL, new String[]{CorreoSYSJM});
                itSend.putExtra(Intent.EXTRA_SUBJECT, new String[]{Asunto+" "+Nombre});
                itSend.putExtra(Intent.EXTRA_TEXT, Obser.getText());

                startActivity(itSend);
            }
        });
    }

}
