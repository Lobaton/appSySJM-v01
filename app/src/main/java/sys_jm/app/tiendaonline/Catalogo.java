package sys_jm.app.tiendaonline;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by schuma on 14/06/15.
 */
public class Catalogo extends AppCompatActivity {

    //String URL = "http://192.168.1.105/Descargas/Migracion/buscar.php";
    String URL = "http://www.toyolobaimport.com/SyS_JM/app/buscar.php";

    Activity a;
    Context context;
    static ArrayList<Producto> lista;
    int CantidadReg;
    JSONArray datos;
    private ListView listViewLista;
    ImageButton imageButton;
    private TextView txtcodigo;
    TextView resultado;
    private Herramienta H;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listado);

        //getActionBar().setDisplayHomeAsUpEnabled(true);
        H = new Herramienta();

        ///verificamos si tenemos conexion ///
        if (!H.verificaConexion(this)) {
            H.MensajeTiempoCorto(this, "Comprueba tu conexión a Internet.");
            this.finish();
        }else{
            ////////// CATALOGO ///////////////
            lista = new ArrayList<Producto>();
            a = this;
            context = getApplicationContext();
            listViewLista = (ListView) findViewById(R.id.listViewLista);
            imageButton = (ImageButton) findViewById(R.id.imageButton);
            txtcodigo = (EditText) findViewById(R.id.txtcodigo);
            resultado = (TextView) findViewById(R.id.resultado);

            new ObtenerProductos(listViewLista).execute();

            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new ObtenerProductos(listViewLista).execute();
                }
            });

            txtcodigo.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
                    if (arg1 == KeyEvent.KEYCODE_ENTER){
                       //Toast.makeText(txtcodigo.getContext(),"enter",Toast.LENGTH_SHORT).show();
                        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                        new ObtenerProductos(listViewLista).execute();
                        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        return true;
                    }
                    return false;
                }
            });

            listViewLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                    //TODO Añadir el código para recuperar la información y mandarla a la segunda Activity.

                    Producto tareaActual = (Producto) adapterView.getItemAtPosition(position);
                    String msg = tareaActual.getDescr() + " - " + tareaActual.getPrecio() + " - " + tareaActual.getOferta();
                    Mensaje(msg);
                }
            });
        }

        ////////

    }

    protected void Mensaje(String mensaje){
        Log.d(mensaje, toString());
        Toast.makeText(a, mensaje, Toast.LENGTH_LONG).show();

    }
    private class ObtenerProductos extends AsyncTask<Void, Void, Void> {
        ListView list;


        private ProgressDialog pDialog;
        public ObtenerProductos(ListView listViewLista) {
            this.list=listViewLista;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Catalogo.this);
            pDialog.setIcon(R.drawable.ic_logo);
            pDialog.setMessage("Buscando ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // CREAMOS LA INSTANCIA DE LA CLASE
            JSONParser sh = new JSONParser();

            String codigo = txtcodigo.getText().toString();
            List params = new ArrayList();
            params.add(new BasicNameValuePair("codigo",codigo));

            String jsonStr = sh.makeServiceCall(URL, JSONParser.GET, params);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    datos = jsonObj.getJSONArray("productos");

                    CantidadReg = datos.length();
                    //Log.d("SYSJM", "Cantidad de Registro "+String.valueOf(CantidadReg));

                    //Toast.makeText(a, datos.length(), Toast.LENGTH_LONG).show();
                    if (datos.length() > 0){
                        lista.clear();
                        // looping through All Equipos
                        for (int i = 0; i < datos.length(); i++) {
                            JSONObject c = datos.getJSONObject(i);

                           // Log.d("SYSJM", String.valueOf(c.getInt("estatus")));
                            if (c.getInt("estatus") == 0 )
                            {
                                Log.d("SYSJM", "Pagina En Mantenimiento");
                                Producto e=new Producto();
                                e.setURLimagen("https://lh3.googleusercontent.com/m3kGXcuXTclQ1RW9ZLIvfZ1ijulWGeZ0hPWT4kKFFrE=w369-h326-no");
                                e.setDescr("Disculpe...");
                                e.setPrecio("Pagina En Mantenimiento");
                                lista.add(e);
                            }else{

                                String descr = c.getString("descr");
                                String precio = c.getString("precio");
                                String imagen = c.getString("idimagen");
                                String oferta = c.getString("oferta");

                                //String imagen = Picasso.with(this).load("http://servidor.com/imagen.jpg").into(imagen);

                                Producto e=new Producto();
                                e.setURLimagen(imagen);
                                e.setDescr(descr);
                                e.setPrecio(precio);
                                //if (oferta.equals("1")){ e.setOferta("** OFERTA **"); }else{ e.setOferta(""); }
                                e.setOferta(oferta);
                                // adding contact to contact list
                                lista.add(e);
                            }
                        }
                    }else{
                        /*Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(facebook));
                        startActivity(intent); */
                        Log.d("SYSJM","No se encontro el producto");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("SYSJM", "Problema al cargar el JSON");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            String texto;
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()){
                pDialog.dismiss();
            }
            new CargarListTask().execute();
            //Log.d("SYSJM", "Cantidad de Registro "+String.valueOf(CantidadReg));

            texto = txtcodigo.getText().toString();
            if (texto.isEmpty()){ texto = "Productos "; }else{ texto = texto.toUpperCase(); }
            resultado.setText(String.valueOf(CantidadReg)+" "+texto+" Encontrados");
        }

        //HILO PARA CARGAR LOS DATOS EN EL LISTVIEW
        class CargarListTask extends AsyncTask<Void,String, sys_jm.app.tiendaonline.Adapter>{
            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
            }

            protected Adapter doInBackground(Void... arg0) {
                // TODO Auto-generated method stub

                try{

                }catch(Exception ex){
                    ex.printStackTrace();
                }

                //Adapter adaptador = new Adapter(a,lista);
                Adapter adaptador = new Adapter(a,lista);
                return adaptador;
            }

            @Override
            protected void onPostExecute(Adapter result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
                listViewLista.setAdapter(result);


            }
        }
    }

}
