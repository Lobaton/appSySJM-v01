package sys_jm.app.tiendaonline;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class WebView_Facebook extends ActionBarActivity {
    String facebook = "https://www.facebook.com/SeguridadySistemaJM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_facebook);

        Herramienta mensj = new Herramienta();
        mensj.MensajeTiempoCorto(this, "Espere ...");

        WebView webView2 = (WebView) findViewById(R.id.webView);
        webView2.setWebViewClient(new WebViewClient());
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.loadUrl(facebook);

    }

    private class webView2 extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
