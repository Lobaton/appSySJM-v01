package sys_jm.app.tiendaonline;

import android.graphics.Bitmap;

/**
 * Created by schuma on 11/06/15.
 */
public class Producto {
    private Bitmap idImagen;
    private String Descr;
    private String Video;
    private String Audio;
    private String Resolucion;
    private String Almacenamiento;
    private String Grabacion;
    private String General;
    private String Precio;
    private String URLimagen;
    private String Oferta;

    public Producto(){
        super();
    }

    public Bitmap getIdImagen(){
        return idImagen;
    }

    public String getDescr(){
        return Descr;
    }

    public String getPrecio(){
        return Precio;
    }

    /*public void setIdImagen(String idImagen) { 
        this.idImagen = idImagen;
    }*/

    public void setDescr(String descr) {
        this.Descr = descr;
    }

    public void setPrecio(String precio) {
        this.Precio = precio;
    }


    public String getURLimagen() {
        return URLimagen;
    }

    public void setURLimagen(String uRLimagen) {
        URLimagen = uRLimagen;
    }

    public String getVideo() {
        return Video;
    }

    public void setVideo(String video) {
        Video = video;
    }

    public String getAudio() {
        return Audio;
    }

    public void setAudio(String audio) {
        Audio = audio;
    }

    public String getResolucion() {
        return Resolucion;
    }

    public void setResolucion(String resolucion) {
        Resolucion = resolucion;
    }

    public String getAlmacenamiento() {
        return Almacenamiento;
    }

    public void setAlmacenamiento(String almacenamiento) {
        Almacenamiento = almacenamiento;
    }

    public String getGrabacion() {
        return Grabacion;
    }

    public void setGrabacion(String grabacion) {
        Grabacion = grabacion;
    }

    public String getGeneral() {
        return General;
    }

    public void setGeneral(String general) {
        General = general;
    }

    public String getOferta() {
        return Oferta;
    }

    public void setOferta(String oferta) {
        Oferta = oferta;
    }
}
