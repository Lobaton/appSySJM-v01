package sys_jm.app.tiendaonline;


import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class webview_twitter extends ActionBarActivity {

    private static final String twitter = "https://twitter.com/SyS_JM";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_twitter);

        Herramienta mensj = new Herramienta();
        mensj.MensajeTiempoCorto(this, "Espere ...");

        WebView webView2 = (WebView) findViewById(R.id.webViewTwitter);
        webView2.setWebViewClient(new WebViewClient());
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.loadUrl(twitter);

    }

    private class webView2 extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
