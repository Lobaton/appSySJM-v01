package sys_jm.app.tiendaonline;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class Adapter extends BaseAdapter{
	  
    protected Activity activity;
    //ARRAYLIST CON TODOS LOS ITEMS
    protected ArrayList<Producto> items;
     
    //CONSTRUCTOR
    public Adapter(Activity activity, ArrayList<Producto> items) {
        this.activity = activity;
        this.items = items;
      }
    //CUENTA LOS ELEMENTOS
    @Override
    public int getCount() {
        return items.size();
    }
    //DEVUELVE UN OBJETO DE UNA DETERMINADA POSICION
    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    //METODO PRINCIPAL, AQUI SE LLENAN LOS DATOS
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // SE GENERA UN CONVERTVIEW POR MOTIVOS DE EFICIENCIA DE MEMORIA
     //ES UN NIVEL MAS BAJO DE VISTA, PARA QUE OCUPEN MENOS MEMORIA LAS
 
        View v = convertView;
        //ASOCIAMOS LA VISTA AL LAYOUT DEL RECURSO XML DONDE ESTA LA BASE DE

        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
       if(convertView == null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_producto, null);
        }

        Producto dir = items.get(position);
        //RELLENAMOS LA IMAGEN Y EL TEXTO
        //IMAGEN
        ImageView img = (ImageView) v.findViewById(R.id.imageView1);
		if(img != null) {
			//new LoadImage(img).execute(dir.getURLimagen());
            Picasso.with(activity).load(dir.getURLimagen()).into(img);
            //String imagen = Picasso.with(context).load(c.getString("idimagen")).resize(50,50).centerCrop().toString();
            //muestre si la ha descargado, sacado de cache o de disco con un pequeño label en una de las esquinas.
            //Picasso.with(activity).setIndicatorsEnabled(true);
		}
        //CAMPOS
        TextView descr = (TextView) v.findViewById(R.id.descr);
        TextView precio = (TextView) v.findViewById(R.id.precio);
        TextView oferta = (TextView) v.findViewById(R.id.oferta);

        descr.setText(dir.getDescr());
        precio.setText(dir.getPrecio());
        oferta.setText(dir.getOferta());

        // DEVOLVEMOS VISTA
        return v;
    }
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	class LoadImage extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public LoadImage(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		@Override
		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				mIcon11 = BitmapFactory.decodeStream((InputStream)new URL(urldisplay).getContent());
				 
			} catch (Exception e) {
				//Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}
	}
}