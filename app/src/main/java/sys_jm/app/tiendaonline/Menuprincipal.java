package sys_jm.app.tiendaonline;

import android.os.Bundle;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.widget.Button;
import android.support.v7.widget.Toolbar;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class Menuprincipal extends AppCompatActivity {

    /*
        Variables globales
        */
    Button btnCatalogo;
    Button btnLlamenos;
    Button btnListaPrecio;
    Button btnFacebook;
    Button btnTwitter, btnContactenos;

    String telmov = "04123021677";
    //private static final String facebook = "https://www.facebook.com/SeguridadySistemaJM";
    //private static final String twitter = "https://twitter.com/SyS_JM";

    //String twitter = "https://twitter.com/SyS_JM";
    //String listaprecio = "http://www.toyolobaimport.com/TLI/uploads/ListaGeneral.pdf"
    String listaprecioPDF = "http://www.toyolobaimport.com/SyS_JM/app/14156.pdf";
    String PagWeb = "http://seguridadsistema.mercadoshops.com.ve";

    String CorreoVenta = "ventas@seguridadsistema.com.ve";
    String MensajeErrorCorreoNoInstalado = "No tiene instalado ningun cliente de correo...";
    String TituloDelCorreo = "Solicitud de Información";

    //String MensajeErrorGeneral = "Ha ocurrido un Error, vuelva a intentarlo...";

    String MensajeCompartirTitulo = "Te Recomiendo esta App";
    String NombreApp = "CCTV - SyS JM";
    String URLApk = "http://bit.ly/SyS_JM";
    String MensajeCompartir = "Hola, te recomiendo esta aplicación que estoy usando llamada <<"+NombreApp+">>, es un Mayorista de Cámara de Seguridad (CCTV) en Venezuela  y Distribuyen a Nivel Nacional... Puedes Descargarlo desde aqui "+URLApk;

    //String URL = "http://192.168.1.105/Descargas/2015-05/buscar.php";
    //String URL = "www.toyolobaimport.com/TLI/uploads/buscar.php";

    Activity a;
    //Context context;
    static ArrayList<Producto> lista;
    //JSONArray pers;
    //private ListView listViewLista;
    String MensajeAcercade = "Version 0.1\n Por favor, pongase en contacto con nosotros para cualquier duda o sugerencia a traves de nuestro correo ventas@seguridadsistema.com.ve";

    private Herramienta H;
    private Toolbar toolbar;                              // Declaring the Toolbar Object
    public Toolbar supportActionBar;
    public Animation anibutt;
    //private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuprincipal);

        H = new Herramienta();
        toolbar = (Toolbar) findViewById(R.id.tool_bar2); // Attaching the layout to the toolbar object
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        //setSupportActionBar(toolbar);

        //botones
        btnCatalogo    = (Button) findViewById(R.id.btnCatalogo);
        btnLlamenos    = (Button) findViewById(R.id.btnLlamenos);
        btnListaPrecio = (Button) findViewById(R.id.btnListaPrecio);
        btnFacebook    = (Button) findViewById(R.id.btnFacebook);
        btnTwitter     = (Button) findViewById(R.id.btnTwitter);
        btnContactenos = (Button) findViewById(R.id.btnContactenos);
    }


    public void onClickContactenos(View view)
    {
        //Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
        //view.startAnimation(anibutt);

        Intent intentCompartir = new Intent(Intent.ACTION_SEND);
        intentCompartir.setType("message/rfc822");
        // intentCompartir.setType("text/plain");
        intentCompartir.putExtra(Intent.EXTRA_EMAIL, new String[]{CorreoVenta});
        intentCompartir.putExtra(Intent.EXTRA_SUBJECT, TituloDelCorreo);

        try {
            startActivity(Intent.createChooser(intentCompartir , "Enviar correo..."));
        } catch (ActivityNotFoundException ex){
            Toast.makeText(Menuprincipal.this, MensajeErrorCorreoNoInstalado, Toast.LENGTH_SHORT).show();
        }

        /*
                AlertDialog alertDialog = new
                AlertDialog.Builder(this).create();
                alertDialog.setTitle("Contactenos");
                alertDialog.setMessage("Nuestro Correo es ventas@seguridadsistema.com.ve");
                alertDialog.setButton("Ok", new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                alertDialog.setIcon(R.drawable.ic_people);
                alertDialog.show();
*/

        /*
        ProgressDialog pDialog = new ProgressDialog(Menuprincipal.this);
        pDialog.setMessage("Pronto estará Disponible...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();*/

    }


    public void onClickCatalogo(View view){
        //Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
        //view.startAnimation(anibutt);
        Intent i = new Intent(this, Catalogo.class);
        startActivity(i);
    }

    public void OnClickTwitter(View view) throws Exception {
        //view.startAnimation(buttonClick);
        //Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
        //view.startAnimation(anibutt);
        Intent intent = new Intent(this, webview_twitter.class);
        startActivity(intent);

    }

    public void OnClickFacebook(View view) throws Exception {
       // Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
        //view.startAnimation(anibutt);
        //Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setData(Uri.parse(facebook));
        //startActivity(intent);

        ////v2
        Intent intent = new Intent(this, WebView_Facebook.class);
        startActivity(intent);
    }

    public void OnClickPaginaWeb(View view) throws Exception {
        //Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
       // view.startAnimation(anibutt);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(PagWeb));
        startActivity(intent);
    }


    public void OnClickListaPrecio(View view) throws Exception {
       // Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
       // view.startAnimation(anibutt);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setData(Uri.parse("http://www.toyolobaimport.com/ML/GraciasPorSuCompra/Images/GraciasPorSuCompra_01.png"));
        intent.setData(Uri.parse(listaprecioPDF));

        //intent.setType("application/png");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // No application to view, ask to download one
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Aplicación No Existe");
            builder.setMessage("Desea Descargar de Android Market?");
            builder.setPositiveButton("Si, Por Favor",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                            marketIntent
                                    .setData(Uri
                                            .parse("market://details?id=com.adobe.reader"));
                            startActivity(marketIntent);
                        }
                    });
            builder.setNegativeButton("No, Gracias", null);
            builder.create().show();
        }
    }

    public void OnClickLlamar(View view){
        try{
           // Animation anibutt = AnimationUtils.loadAnimation(this,R.anim.anim_button);
          //  view.startAnimation(anibutt);
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:"+telmov));
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("dialing-example", "Call failed", activityException);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_acercade, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        /*if (id == R.id.action_settings) {
            VerificarNuevaBD();
            //mensaje = "555555";
            //Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
            // return true;
            */
        if (id == R.id.compartir) {
            ///Compartir compartir = new Compartir(Menuprincipal.this, MensajeCompartirTitulo, MensajeCompartir);
            H.Compartir(this, H.MensajeCompartirTitulo, H.MensajeCompartir);


        }else if (id == R.id.acercade) {
            //getAcercade();
            Intent i = new Intent(this, Acercade.class);
            startActivity(i);

        }else if (id == R.id.sugerencia) {
            Intent i = new Intent(this, Sugerencia.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    public void setSupportActionBar(Toolbar supportActionBar) {
        this.supportActionBar = supportActionBar;
    }

/*
    public void getCompartir(View view){
        Compartir compartir = new Compartir(Menuprincipal.this, MensajeCompartirTitulo, MensajeCompartir);

    }
*/
    public void getAcercade(){
        AlertDialog alertDialog = new
                AlertDialog.Builder(this).create();
        alertDialog.setTitle("Acerca de...");
        alertDialog.setMessage(MensajeAcercade);
        alertDialog.setButton("Ok", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        alertDialog.setIcon(R.drawable.ic_logo);
        alertDialog.show();

    }


    private void VerificarNuevaBD() {
        //  manager = new DataBaseManager(this);
        //  manager.copyDataBase(this);
    }

}
